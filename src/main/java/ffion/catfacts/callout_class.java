package ffion.catfacts;

import java.net.http.*;
import java.net.http.HttpResponse.BodyHandlers;
import java.io.IOException;
import java.net.URI;

public class callout_class {

    public HttpResponse<String> new_callout() throws IOException, InterruptedException{
    // instantiate new HttpClient
    HttpClient client = HttpClient.newHttpClient();

    // build the request
    HttpRequest req = HttpRequest.newBuilder().uri(URI.create("https://catfact.ninja/fact")).build();

    // instantiate and return HttpResponse object after sending request. HttpResponse object cannot be raw. BodyHandlers.ofString tells server we want to receive the request as a string
    HttpResponse<String> response = client.send(req,BodyHandlers.ofString());

    return response;
    
    }

}
