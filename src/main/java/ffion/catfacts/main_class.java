package ffion.catfacts;

import java.io.IOException;
import java.net.http.HttpResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

// import com.fasterxml.jackson.databind.ObjectMapper;

public class main_class{

    public static void main(String[] args) throws IOException, InterruptedException {

        callout_class callout_instance = new callout_class();
        HttpResponse<String> response = callout_instance.new_callout();

        // print the status code
        System.out.println(response.statusCode());

        // print the body of the response
        System.out.println(response.body());

        // String json_attribute = "{ \"country\" : \"United Kingdom\" }";

        // Instantiate the parse_JSON class
        parse_JSON parse_JSON_instance = new parse_JSON();
        
        // use the parse_JSON instance to create objects we will use to readValue
        ObjectMapper obj_ObjectMapper = parse_JSON_instance.newObjectMapper();
        catfacts_Bean obj_Catfacts_Bean = parse_JSON_instance.newCatfacts_Bean();

        // readValue of JSON. **We are deserializing the body of the response, instead of the response itself
        obj_Catfacts_Bean = obj_ObjectMapper.readValue(response.body(), catfacts_Bean.class);

        // use the getter/setter methods from catfacts_Bean to print out different attributes
        System.out.println("catfact presents the following tidbit: " + obj_Catfacts_Bean.getFact());

    }



}